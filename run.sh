#!/bin/sh

USER_TARGET=$1

if [ -z "$1" ]; then
    echo "$0 [user]"
    exit 1
fi
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
if ! id -u $USER_TARGET > /dev/null 2>&1; then
    echo "The user does not exist"
    exit 1
fi

apt-get update
apt-get upgrade
apt-get install -y git vim xorg bspwm sudo zsh numlockx rxvt-unicode fonts-inconsolata
sudo adduser $USER_TARGET sudo
cd /home/$USER_TARGET
git clone https://hhabbak@gitlab.com/hhabbak/dotfiles.git
ln -s ./dotfiles/.zshrc
ln -s ./dotfiles/.vim
ln -s ./dotfiles/.vimrc
ln -s ./dotfiles/.vimrc-plugins
ln -s ./dotfiles/.Xresources
chsh -s /bin/zsh $USER_TARGET
cp ./dotfiles/.xinitrc .
mkdir -p .config/bspwm
mkdir -p .config/sxhkd
cp /usr/share/doc/bspwm/examples/bspwmrc .config/bspwm
cp /usr/share/doc/bspwm/examples/sxhkdrc .config/sxhkd
chown $USER_TARGET:$USER_TARGET -R .
